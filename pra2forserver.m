
fprintf('pra2forserver.m is running\n')

global bsefilefolder

intialpwd=pwd;
bsefilefolder='~/bsenew/';
fprintf(datestr(now));
jianmass=1;
maxmass=150;
f=0.1; % or 0.8
% rate of two intial mass /binary fractionclear
Ss=(14.2-14.2.*f)./(0.144-0.045.*f); % formation rates of single  star populastions
Sb=7.1.*f./(0.144-0.045.*f);% formation rates of binary star populastions
Ss=Ss*3.5/7.1;Sb=Sb*3.5/7.1; % only for modol 3
maxtime=300*10^6; % or 300Myr
G=6.67259e-11;
% maxium of the years taking into account
Msun=1.9891*10^30; % kg
Rsun=6.955*10^5;
deltaT=60.*60*12*365*1e3;
alpha=1;
edd=10;
% input parameters for analysisforpra2
Lsun=3.845*10^33;
logmin=log10(10^36/Lsun);
resseafterL=[];
rebseafterL=[];
ii=0;
%% basic caculation

massall=jianmass:jianmass:maxmass;
% massrate=jianmass.*cumsum(massfunction(massall));
rebseafterLsavename=['pra2serverresults/rebseafterLM2',datestr(now)];
Msun=1.9891*10^30; % kg

ii=0
numOfrebseafterL=0;
for mass=massall
    
    for mass2=mass:jianmass:massall(end)

        %%% 运行bse程序
        bseimport(mass,mass2,alpha,edd,maxtime/10^6);
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%
        

        %% 提取并筛选 
        rebse=importdata([bsefilefolder,'binary.dat']);


        % save( [filefoldername,num2str(mass),'-',num2str(mass2)],'rebse')
        if(rebse(end,1)<0)
            rebse=rebse(1:end-1,:);
        end
        
        
        
        %{
            Rstar=Rsun*10.^(rebse(:,8:9));
            aa=Rstar(:,1);
            Rstar(:,1)=Rstar(:,2);
            Rstar(:,2)=aa;
            deltaT=60.*60*12*365*1e3;
            radiation=(G*(Msun.*rebse(:,4:5)).^2./Rstar.*10.^rebse(:,16:17).*1e7);
            radiation2=max(radiation,[],2);
        %}

        Rstar=Rsun*10.^(rebse(:,8:9));

        %% 区分主星和伴星
        nuwei=double(rebse(2)<rebse(3));
        %% 引力势能转化为 X射线能量
        radiation=(G*(Msun.*rebse(:,4+nuwei)).^2./Rstar(:,1+nuwei).*rebse(:,16+nuwei).*1e7)./deltaT;
        [len,wid]=size(rebse);
        if(len<5)
            continue;
        end
        rebse=[rebse,mass*ones(len,1),mass2*ones(len,1)];
        rebse=[rebse,[inf;(rebse(3:end,1)-rebse(1:end-2,1))./2;inf]];
        rebse=[rebse,radiation];
        
        % rebse=[rebse,Msun.*rebse(:,16).*rebse(:,4),Msun.*rebse(:,17).*rebse(:,5)];
        
        % rebse1=rebse(rebse(:,12)>logmin | rebse(:,13)>logmin,:);
        % rebse1=[rebse1;rebse(rebse(:,13)>logmin,:)];
        
        %% 筛选掉不要的数据
        rebse=rebse(rebse(:,2)>=13 | rebse(:,3)>=13,:);
        rebse=rebse(rebse(:,2)<15 & rebse(:,3)<15,:);
        rebse=rebse(rebse(:,2)>2 & rebse(:,3)>2,:);
        rebseafterL=[rebseafterL;rebse];
        [a,~]=size(rebseafterL);
        %% 分批储存
        if(a>=5e5)
            numOfrebseafterL=numOfrebseafterL+1;
            save([bsefilefolder,rebseafterLsavename,...
                num2str(numOfrebseafterL)],'rebseafterL');
            rebseafterL=[];
            
        end
    end
    ii=ii+1;
    fprintf(num2str(ii))
end
fprintf ('\n Caculation is finished\n')
numOfrebseafterL=numOfrebseafterL+1;
save([bsefilefolder,rebseafterLsavename,...
    num2str(numOfrebseafterL)],'rebseafterL');
save([bsefilefolder,rebseafterLsavename,'Alldata']);

%{
for mass1=massall
for mass2=mass1:10:20
bseimport(mass1,mass2,0.1,10,100);
result=importdata([bsefilefolder,'binary.dat']);
save('try1','result');
end
end
 
%}

cd(intialpwd)

fprintf(datestr(now));
fprintf('All finished\n')
exit();







%% mass function for intial mass
%{
 
function rate=massfunction(mm)

    % parameter in mass function. page 19 in bse essay
    m0=0.1;
    a1=0.29056;
    a2=0.15571;
    rate=[];
    for m=mm
    if(m<=m0)
        rate=[rate,zeros(size(m))];
        
    else
        if(m<=0.5)
            rate=[rate,a1.*m.^-1.3];
            
        else
            if(m<=1)
                rate=[rate,a2.*m.^-2.2];
            
            else
                rate=[rate,a2.*m.^-2.7];
                
            end
        end
    end
    end
    end
%}